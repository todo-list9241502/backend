import { userModel } from '../models/user.model';
import { User as UserEntity } from '../entities/user.entity';
import { IUser } from '../interfaces/user.interface';
import { NotFoundError } from '../../../utils/errorHandler';
import { UserCache } from '../redis/user.redis';

export class User {
    private UserModel;
    private UserCache;

    constructor() {
        this.UserModel = userModel;
        this.UserCache = new UserCache();
    }

    public getOne = async (condition: object): Promise<IUser> => {
        return await this.UserModel.getOne(condition);
    };

    public getAll = async (): Promise<IUser[]> => {
        return await this.UserModel.getAll();
    };

    public create = async (user: IUser): Promise<IUser> => {
        const { account, password } = user;
        const userEntity = new UserEntity();
        userEntity.account = account;
        userEntity.password = password;
        return await this.UserModel.create(userEntity);
    };

    public update = async (id: string, user: IUser): Promise<IUser> => {
        return await this.UserModel.update(id, user);
    };

    public delete = async (id: string): Promise<IUser> => {
        return await this.UserModel.delete(id);
    };

    public signin = async (data: IUser, session: any, sessionId: string): Promise<IUser> => {
        const { account, password } = data;
        
        const user = await this.getOne({account, password});
        if (!user) {
            throw new NotFoundError('User not found');
        }

        if (!session.user) {
            session.user = user;
        }

        return user;
    };
};