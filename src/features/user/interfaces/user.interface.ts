export interface IUser {
    id?: string;
    account: string;
    password: string;
    createdAt?: Date;
    updatedAt?: Date;
};