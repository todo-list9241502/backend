import express, { Router } from 'express';
import { userController } from '../controllers/user.controller';

class UserRoutes {
    private router: Router;

    constructor() {
        this.router = express.Router();
    }

    public routes(): Router {
        this.router.get('/user/:id', userController.getOne);
        this.router.get('/user', userController.getAll);
        this.router.post('/user', userController.create);
        this.router.put('/user/:id', userController.update);
        this.router.delete('/user/:id', userController.delete);
        this.router.post('/user/signin', userController.signin);
        return this.router;
    };
}

export const userRoutes: UserRoutes = new UserRoutes();