import { dataSource } from '../../../setupDatabase';
import { User } from '../entities/user.entity';
import { IUser } from '../interfaces/user.interface';

class UserModel {
    private userRepositroy;

    constructor(dataSource: any, entity: any) {
        this.userRepositroy = dataSource.getRepository(entity);
    }

    public getOne = async (condition: object): Promise<IUser> => {
        return await this.userRepositroy.findOneBy(condition);
    };

    public getAll = async (): Promise<IUser[]> => {
        return await this.userRepositroy.find();
    };

    public create = async (userEntity: IUser): Promise<IUser> => {
        return await this.userRepositroy.save(userEntity);
    };

    public update = async (id: string, userEntity: IUser): Promise<IUser> => {
        return await this.userRepositroy.update(id, userEntity);
    };

    public delete = async (id: string): Promise<IUser> => {
        return await this.userRepositroy.delete(id);
    };
}

export const userModel = new UserModel(dataSource, User);