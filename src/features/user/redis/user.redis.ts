import { BaseCache } from "../../../setupCache";
import { IUser } from "../interfaces/user.interface";

export class UserCache extends BaseCache {
    constructor() {
        super();
    }

    public setUser = async (sessionId: string, user: IUser) => {
        if (!this.client.isOpen) {
            await this.client.connect();
        }
        await this.client.HSET(`user:${sessionId}`, 'user', JSON.stringify(user));
    }

    public getUser = async (sessionId: string): Promise<IUser> => {
        if (!this.client.isOpen) {
            await this.client.connect();
        }
        const user = await this.client.HGET(`user:${sessionId}`, 'user');
        return JSON.parse(user as string);
    }
};