import HTTP_STATUS from 'http-status-codes';
import { SuccessResponse } from '../../../utils/response';
import { Request, Response, NextFunction } from "express";
import { User } from '../services/user.service';

class UserController {
    private User;

    constructor() {
        this.User = new User();
    }

    public getOne = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const user = await this.User.getOne({id: req.params.id});
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('Get user successfully', user));
        } catch (error) {
            next(error);
        }
    };

    public getAll = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const users = await this.User.getAll();
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('Get users successfully', users));
        } catch (error) {
            next(error);
        }
    };

    public create = async (req: Request, res: Response, next: NextFunction): Promise<void> =>  {
        try {
            const user = await this.User.create(req.body);
            res.status(HTTP_STATUS.CREATED).json(new SuccessResponse('signin successfully', user));
        } catch (error) {
            next(error);
        }
    };

    public update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const user = await this.User.update(req.params.id, req.body);
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('signin successfully', user));
        } catch (error) {
            next(error);
        }
    };

    public delete = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const user = await this.User.delete(req.params.id);
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('signin successfully', user));
        } catch (error) {
            next(error);
        }
    };

    public signin = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const user = await this.User.signin(req.body, req.session, req.sessionID);
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('signin successfully', user));
        } catch (error) {
            // next(error);
            res.status(HTTP_STATUS.NOT_FOUND).json(error);
        }
    };
};

export const userController: UserController = new UserController();
