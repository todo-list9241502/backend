import HTTP_STATUS from 'http-status-codes';
import { SuccessResponse } from '../../../utils/response';
import { Request, Response, NextFunction } from "express";
import { Todolist } from '../services/todolist.service';

class TodolistController {
    private todoList;

    constructor() {
        this.todoList = new Todolist();
    }

    public getOne = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const todo = await this.todoList.getOne({id: req.params.id});
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('Get todo successfully', todo));
        } catch (error) {
            next(error);
        }
    };

    public getAll = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const todos = await this.todoList.getAll(req.session);
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('Get todos successfully', todos));
        } catch (error) {
            next(error);
        }
    };

    public create = async (req: Request, res: Response, next: NextFunction): Promise<void> =>  {
        try {
            const todo = await this.todoList.create(req.body, req.session);
            res.status(HTTP_STATUS.CREATED).json(new SuccessResponse('Create todo successfully', todo));
        } catch (error) {
            next(error);
        }
    };

    public update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const todo = await this.todoList.update(req.params.id, req.body);
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('Update todo successfully', todo));
        } catch (error) {
            next(error);
        }
    };

    public delete = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            const todo = await this.todoList.delete(req.params.id);
            res.status(HTTP_STATUS.OK).json(new SuccessResponse('Delete todo successfully', todo));
        } catch (error) {
            next(error);
        }
    };
};

export const todolistController = new TodolistController();