import { ITodolist } from "../interfaces/todolist.interface";
import { IUser } from "../../user/interfaces/user.interface";
import { TodoList as TodoListEntity } from '../entities/todolist.entity';
import { todolistModel } from "../models/todolist.model";

export class Todolist {
    private todoListModel;

    constructor() {
        this.todoListModel = todolistModel;
    };

    public getOne = async (condition: object): Promise<ITodolist> => {
        return await this.todoListModel.getOne(condition);
    };

    public getAll = async (session: any): Promise<ITodolist[]> => {
        const { user } = session;
        return await this.todoListModel.getAll(
            {
                relations: ['user'],
                where: {
                  'user.id': user.id,
                },
            }
        );
    };

    public create = async (todoList: ITodolist, session: any): Promise<ITodolist> => {
        const { content, isDone } = todoList;
        const { user } = session;
        const todoListEntity = new TodoListEntity();
        todoListEntity.content = content;
        todoListEntity.isDone = isDone;
        todoListEntity.user = user;
        return await this.todoListModel.create(todoListEntity);
    };

    public update = async (id: string, todoList: ITodolist): Promise<ITodolist> => {
        return await this.todoListModel.update(id, todoList);
    };

    public delete = async (id: string): Promise<ITodolist> => {
        return await this.todoListModel.delete(id);
    };
};