import { dataSource } from '../../../setupDatabase';
import { TodoList } from '../entities/todolist.entity';
import { ITodolist } from '../interfaces/todolist.interface';

class TodolistModel {
    private todolistRepositroy;

    constructor(dataSource: any, entity: any) {
        this.todolistRepositroy = dataSource.getRepository(entity);
    }

    public getOne = async (condition: object): Promise<ITodolist> => {
        return await this.todolistRepositroy.findOneBy(condition);
    };

    public getAll = async (condition: object): Promise<ITodolist[]> => {
        return await this.todolistRepositroy.find(condition);
    };

    public create = async (todolistEntity: ITodolist): Promise<ITodolist> => {
        return await this.todolistRepositroy.save(todolistEntity);
    };

    public update = async (id: string, todolistEntity: ITodolist): Promise<ITodolist> => {
        return await this.todolistRepositroy.update(id, todolistEntity);
    };

    public delete = async (id: string): Promise<ITodolist> => {
        return await this.todolistRepositroy.delete(id);
    };
};

export const todolistModel = new TodolistModel(dataSource, TodoList);