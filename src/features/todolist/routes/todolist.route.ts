import { Router } from 'express';
import { todolistController } from '../controllers/todolist.controller';

class TodolistRoute {
    private router: Router;

    constructor() {
        this.router = Router();
    }

    public routes(): Router {
        this.router.get('/todolist/:id', todolistController.getOne);
        this.router.get('/todolist', todolistController.getAll);
        this.router.post('/todolist', todolistController.create);
        this.router.put('/todolist/:id', todolistController.update);
        this.router.delete('/todolist/:id', todolistController.delete);
        return this.router;
    };
};

export const todolistRoute: TodolistRoute = new TodolistRoute();