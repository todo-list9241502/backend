export interface ITodolist {
    id?: string;
    content: string;
    isDone: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}