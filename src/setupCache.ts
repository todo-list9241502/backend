import { createClient } from 'redis';
import { config } from './config';

export type RedisClient = ReturnType<typeof createClient>;

export class BaseCache {
  protected client: RedisClient;

  constructor() {
    this.client = createClient({ url: config.REDIS_HOST });
  }
}