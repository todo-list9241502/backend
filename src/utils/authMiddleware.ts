import { Request, Response, NextFunction } from 'express';
import { NotAuthoriedError } from './errorHandler'; 

class AuthMiddleware {
    public verifyUser = (req: any, res: Response, next: NextFunction): void => {
        if (req.session.user) {
            next();
        } else {
            throw new NotAuthoriedError();
        }
    };
}

export const authMiddleware: AuthMiddleware = new AuthMiddleware();