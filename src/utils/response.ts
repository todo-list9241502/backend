import HTTP_STATUS from 'http-status-codes';

class Response {
    public statusCode: number;
    public message: string;
    public result: any;

    constructor(
        statusCode: number,
        message: string,
        data: any
    ) {
        this.statusCode = statusCode;
        this.message = message;
        this.result = data;
    }
}

export class SuccessResponse extends Response {
    constructor(message: string = '', data: any = null) {
        super(HTTP_STATUS.OK, message, data);
    }
}
