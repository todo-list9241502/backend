import HTTP_STATUS from 'http-status-codes';

export class CostumerError extends Error {
    public statusCode: number;
    public message: string;
    public result: null;

    constructor(message: string) {
        super(message);
    }

    serializeErrors() {
        return {
            statusCode: this.statusCode,
            message: this.message,
            result: this.result,
        };
    }
}

export class NotFoundError extends CostumerError {
    constructor(message: string = 'Not found') {
        super(message);
        this.statusCode = HTTP_STATUS.NOT_FOUND;
        this.message = message;
        this.result = null;
    }
}

export class NotAuthoriedError extends CostumerError {
    constructor(message: string = 'Unauthorized') {
        super(message);
        this.statusCode = HTTP_STATUS.UNAUTHORIZED;
        this.message = message;
        this.result = null;
    }
}