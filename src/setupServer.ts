import RedisStore from "connect-redis";
import { createClient } from "redis";
import session from 'express-session';
import cors from 'cors';
import HTTP_STATUS from 'http-status-codes';
import { Application, json, urlencoded, Request, Response, NextFunction } from 'express';
import { config } from './config';
import applicationRoutes from './routes';
import { CostumerError } from './utils/errorHandler';

export class TodolistServer {
    private app: Application;

    constructor(app: Application) {
        this.app = app;
    }

    public start(): void {
        this.securityMiddleware(this.app);
        this.standardMiddleware(this.app);
        this.routeMiddleware(this.app);
        this.errorHandler(this.app);
        this.startHttpServer(this.app);
    }

    private securityMiddleware(app: Application): void {
        let redisClient = createClient({ url: config.REDIS_HOST });
        redisClient.connect().catch(console.error)
        let redisStore = new RedisStore({
            client: redisClient,
            prefix: "todolist:",
        })
        app.use(
            session({
                store: redisStore,
                secret: 'mySecret',
                name: 'sessionId',
                saveUninitialized: false,
                resave: false,
                cookie: { maxAge: 600000 }
            })
        );
        this.app.use(
            cors({
                origin: config.CLIENT_HOST,
                credentials: true,
                methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']
            })
        );
    };

    private standardMiddleware(app: Application): void {
        app.use(json());
        app.use(urlencoded({ extended: true }));
    };

    private routeMiddleware(app: Application): void {
        applicationRoutes(app);
    };

    private errorHandler(app: Application): void {
        app.all('*', (req: Request, res: Response) => {
            res.status(HTTP_STATUS.NOT_FOUND).json({ message: `${req.originalUrl} not found` });
        });

        app.use((err: CostumerError, _req: Request, res: Response, next: NextFunction) => {
            console.log(err);
            if (err instanceof CostumerError) {
                return res.status(err.statusCode).json(err.serializeErrors());
            }

            const intervalServerError = new CostumerError((err as any).message);
            return res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json(intervalServerError.serializeErrors());
        });
    };

    private startHttpServer(app: Application): void {
        app.listen(config.PORT, () => {
            console.log(`Server is running on port ${config.PORT}`);
        });
    };
};