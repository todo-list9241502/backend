import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1705236469040 implements MigrationInterface {
    name = 'Mig1705236469040'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "todo_list" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "content" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "user_id" uuid, CONSTRAINT "PK_1a5448d48035763b9dbab86555b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "todo_list" ADD CONSTRAINT "FK_875f735cfedb1c34d69670b2869" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "todo_list" DROP CONSTRAINT "FK_875f735cfedb1c34d69670b2869"`);
        await queryRunner.query(`DROP TABLE "todo_list"`);
    }

}
