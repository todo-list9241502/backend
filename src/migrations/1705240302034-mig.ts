import { MigrationInterface, QueryRunner } from "typeorm";

export class Mig1705240302034 implements MigrationInterface {
    name = 'Mig1705240302034'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "todo_list" ADD "is_done" boolean NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "todo_list" DROP COLUMN "is_done"`);
    }

}
