import express, { Express } from 'express';
import { config } from './config';
import { connectDatabase } from './setupDatabase';
import { TodolistServer } from './setupServer';

class Application {
  public initialize(): void{
    this.loadConfig();
    this.loadDatabase();
    this.startServer();
  }

  private loadConfig(): void {
    config.validateConfig();
  }

  private loadDatabase(): void {
    connectDatabase();
  };

  private startServer(): void {
    const app: Express = express();
    const server: TodolistServer = new TodolistServer(app);
    server.start();
  };
};

const appliaction: Application = new Application();
appliaction.initialize();
