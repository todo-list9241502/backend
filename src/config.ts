import dotenv from 'dotenv';
dotenv.config();

class Config {
    public PORT: string | undefined;
    public ENVIRONMENT: string | undefined;
    public CLIENT_HOST: string | undefined;
    public DB_TYPE: string | undefined;
    public DB_HOST: string | undefined;
    public DB_PORT: string | undefined;
    public DB_USERNAME: string | undefined;
    public DB_PASSWORD: string | undefined;
    public DB_DATABASE: string | undefined;
    public REDIS_HOST: string | undefined;

    constructor() {
        this.PORT = process.env.PORT;
        this.ENVIRONMENT = process.env.ENVIRONMENT;
        this.CLIENT_HOST = process.env.CLIENT_HOST;
        this.DB_TYPE = process.env.DB_TYPE;
        this.DB_HOST = process.env.DB_HOST;
        this.DB_PORT = process.env.DB_PORT;
        this.DB_USERNAME = process.env.DB_USERNAME;
        this.DB_PASSWORD = process.env.DB_PASSWORD;
        this.DB_DATABASE = process.env.DB_DATABASE;
        this.REDIS_HOST = process.env.REDIS_HOST;
    }

    public validateConfig() {
        for (const [key, value] of Object.entries(this)) {
            if (value === undefined) {
                throw new Error(`Configuration ${key} is undefined.`);
            }
        }
    }
}

export const config = new Config();