import { Application } from 'express';
import { authMiddleware } from './utils/authMiddleware';
import { healthRoutes } from './healthRoutes';
import { userRoutes } from './features/user/routes/user.route';
import { todolistRoute } from './features/todolist/routes/todolist.route';

const BASE_PATH = '/api/v1';

export default (app: Application) => {
    const routes = () => {
        app.use('', healthRoutes.health());
        app.use('', healthRoutes.env());

        app.use(BASE_PATH, userRoutes.routes());
        app.use(BASE_PATH,  authMiddleware.verifyUser, todolistRoute.routes());
    }
    routes();
};