FROM node:20-alpine as builder
WORKDIR /server
COPY package*.json tsconfig*.json ./
COPY ./src ./src
RUN npm install && npm run build

FROM node:20-alpine
WORKDIR /server
COPY package*.json ./
RUN npm install
COPY --from=builder /server/build /server
EXPOSE 3030
CMD npm run migration:prd:run && npm run start:prd